var tmap = L.map('main_map').setView([4.5961018,-74.0759909], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(tmap);

L.marker([4.5971917,-74.0766225]).addTo(tmap);
L.marker([4.5997749,-74.0749526]).addTo(tmap);


$.ajax({
	dataType: "json",
	url: "api/bicicletas",
	success: function(result){
		console.log(result);
		result.bicicletas.forEach(function(bici){
			L.marker(bici.ubicacion, {title: bici.id}).addTo(tmap);
		});
	}
})
