var map = L.map('main_map').setView([-6.745417, -79.702000], 20);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(map);
//L.marker([51.5, -0.09]).addTo(map);

$.ajax({
    dataType: "json",
    url: "api/bicicletas",
    success: function(result){
        result.bicicletas.forEach(function(bici){
            L.marker(bici.ubicacion,{title:bici.id}).addTo(map);
        })
    }
})