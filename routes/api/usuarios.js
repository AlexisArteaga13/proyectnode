var express = require("express");
var router = express.Router();
var usuarioController = require('../../controladores/api/usuarioControladorAPI');

router.get('/', usuarioController.usuarios_list); /* ruta base de las bicicletas| listado de bicicletas*/
router.post('/create', usuarioController.usuarios_create);
router.post('/reservar', usuarioController.usuario_reservar);

module.exports = router;