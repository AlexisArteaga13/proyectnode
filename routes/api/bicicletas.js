var express = require("express");
var router = express.Router();
var bicicletaController = require('../../controladores/api/bicicletaControladorAPI');

router.get('/', bicicletaController.bicicleta_list); /* ruta base de las bicicletas| listado de bicicletas*/
router.post('/create', bicicletaController.bicicleta_create);
router.delete('/delete', bicicletaController.bicicleta_delete);
router.post('/:id/update', bicicletaController.bicicleta_update_post);

module.exports = router;


