/* modulo de rutas de express */
var express = require("express");
var router = express.Router();
var bicicletaController = require('../controladores/bicicleta');

router.get('/', bicicletaController.list); /* ruta base de las bicicletas| listado de bicicletas*/
router.get('/crear', bicicletaController.bicicleta_crear_get);
router.post('/crear', bicicletaController.bicicleta_crear_post);
router.get('/:id/update', bicicletaController.bicicleta_update_get);
router.post('/:id/update', bicicletaController.bicicleta_update_post);
router.post('/:id/delete', bicicletaController.bicicleta_delete_post); /* :id parametro */

module.exports = router;