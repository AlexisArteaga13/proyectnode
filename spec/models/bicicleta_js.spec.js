///
var mongoose = require('mongoose');
var bicicleta = require("../../models/bicicleta");
beforeEach(() => console.log("testeando..."));
beforeEach(() => {bicicleta.allBicis = [];});
describe('Bicicleta.CreateInstance',()=>{
    it('Crea una instancia de Bicicleta',()=>{
        var bici = bicicleta.createInstance(1,"verde","urbana",[-34.5,-34.1]);
        expect(bici.code).toBe(1);
        expect(bici.color).toBe("verde");
        expect(bici.modelo).toBe("urbana");
        expect(bici.ubicacion[0]).toEqual(-34.5);
        expect(bici.ubicacion[0]).toEqual(-34.1);
        

    })
})
describe('testing Bicicletas',function(){
    beforeEach(function(done){
        var mongoDB='mongodb://localhost/testdb';
        mongoose.connect(mongoDB,{useNewUrlParser:true});
        const db = mongoose.connection;
        db.on('error',console.error.bind(console,'connection error'));
        db.once('open',function(){
            console.log('We are connected to test database:');
            done();
        });
    });
    afterEach(function(done){
        bicicleta.deleteMany({},function(err,success){
            if(err) console.log(err);
            done();                                                                                
        })
    })
})

describe("bicicleta.allBicis", function() {
    it("comienza en vacio", function(err,bicis){
    expect(bicis.length).toBe(0);
    done();
    });
   }); 

   

// PARA TESTEAR EL ADD BICICLETA
/*
describe("bicicleta.add", function() {
    it("Agregamos uno", function(){
    expect(bicicleta.allBicis.length).toBe(0);
    var a = new bicicleta(1,'rojo','urbana',[-6.7453772, -79.7021130]);
    bicicleta.add(a);
    expect(bicicleta.allBicis.length).toBe(1);
    expect(bicicleta.allBicis[0]).toBe(a);
    });
   }); 

   // PARA TESTEAR EL FIND BY BICICLETA
describe("bicicleta.findById", function() {
    it("buscar uno", function(){
    expect(bicicleta.allBicis.length).toBe(0);
    var a = new bicicleta(1,'rojo','urbana',[-6.7453772, -79.7021130]);
    var b = new bicicleta(2,'blanco','urbana',[-6.745428, -79.702010]);
    bicicleta.add(a);
    bicicleta.add(b);
    expect(bicicleta.allBicis.length).toBe(2);
    expect(bicicleta.allBicis[0]).toBe(a);
    var targetbici  = bicicleta.findById(1);
    expect(targetbici.id).toBe(1);
    expect(targetbici.color).toBe(a.color);

    expect(targetbici.modelo).toBe(a.modelo);
    

    });
   }); */