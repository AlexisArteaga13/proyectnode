var Usuario = require('../models/usuario');
var Token = require('../models/token');

module.exports = {
	confirmationGet: function(req,res,next){
		Token.findOne({token: req.params.token}, function (err,token){
			if (!token) return res.status(400).send({type : 'not-verified', msg: 'No se ha verificado el correo mediante token. Quiza haya expirado y debas solicitarlo de nuevo'});
			Usuario.findById(token._userId, function(err, Usuario){
				if(!Usuario) return res.status(400).send({msg: 'No encontramos un usuario con este token '});
				if (Usuario.verificado) return res.redirect('/usuarios');
				Usuario.verificado = true;
				Usuario.save(function (err){
					if(err) {return res.status(500).send({msg: err.message}); }
					res.redirect('/')
				});
			});
		});
	},
}