var Bicicleta = require('../../models/bicicleta');

exports.bicicleta_list = function(req,res){
	Bicicleta.find({}, function(err, bicicletas){
		res.status(200).json({
			bicicletas:bicicletas
		});
	});
};

exports.bicicleta_create = function(req,res){
	var bici = new Bicicleta({code: req.body.code, color: req.body.color, modelo: req.body.modelo});
	bici.ubicacion = [req.body.lat, req.body.lng];


	//(req.body.id, req.body.color, req.body.modelo)
	//bici.ubicacion = [req.body.lat, req.body.lng];
	bici.save(function(err){
		res.status(200).json(bici);
	});

	//Bicicleta.add(bici);

	/*res.status(200).json({
		bicicleta: bici
	});*/
};

exports.bicicleta_delete = function(req,res){
	//Bicicleta.removeByCode(req.body.code);
	Bicicleta.removeByCode(req.body.id, function(err, bicicletas){
		console.log(bicicletas);
		res.status(204).send();	
	});	
};

exports.bicicleta_update_post = function(req,res){
	var bici = Bicicleta.findByCode(req.params.id);
	bici.id = req.body.id;
	bici.color = req.body.color;
	bici.modelo = req.body.modelo;
	bici.ubicacion = [req.body.lat, req.body.lng];

	res.status(200).json({
		bicicleta: bici
	});

}
