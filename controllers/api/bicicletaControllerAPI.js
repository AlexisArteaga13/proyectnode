/*var bicicleta = require('../../models/bicicleta');
const { allBicis } = require('../../models/bicicleta');
const { bicicleta_update_get } = require('../bicicleta');
exports.bicicleta_list = function(req,res){
    res.status(200).json({
        bicicletas: bicicleta.allBicis
    })
}

exports.bicicleta_create = function (req,res){
    var bici= new bicicleta(req.body.id,req.body.color,req.body.modelo);
    bici.ubicacion=[req.body.lat, req.body.lng];
    bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    })
}
exports.bicicleta_delete=function(req,res){
    bicicleta.removeById(req.body.id);
    res.status(204).send();
}*/
var Bicicleta = require ('../../models/bicicleta');

exports.bicicleta_list = function (req, res){
    Bicicleta.allBicis(function(err,bicis){
        res.status(200).json({
            bicicletas : bicis
        });
    }) 
}

exports.bicicleta_create = function (req, res){
    var bici = Bicicleta.createInstance(req.body.code, req.body.color,req.body.modelo);
    bici.ubicacion=[req.body.lat, req.body.lng];
    console.log(bici);
    Bicicleta.add(bici, function(err,newBici){
        res.status(200).json({
            bicicleta : bici
        })
    });
}

exports.bicicleta_delete =function(req, res){
    Bicicleta.removeByCode(req.params.code, function(err, bici){
        res.status(204).send();
    }) 
}